<?php 
	require_once('funciones/funciones.php');
?>

<!DOCTYPE html>
<html>
<head>
	<title>Notepass</title>
	<link rel="stylesheet" type="text/css" href="css/estilo.css">
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script type="text/javascript">

		$(document).ready(function(){
			//alert(sessionStorage.nombre);
         	//alert(sessionStorage.idfacebook);
          	//alert(sessionStorage.email);
          	//document.getElementById('user-nameFacebook').innerHTML = sessionStorage.nombre;
			document.getElementById('user-picture').innerHTML = '<img src="http://graph.facebook.com/'+sessionStorage.idfacebook+'/picture" class="rounded"/>';
			
			$('#referidos').click(function(){
				//$('#referidos').css('display','none');	
				$('#referidos h3').css('display','none');	
				$('#referidos a').css('display','none');	
				$('#referidos').css('padding','0px');	
				$('#referidos').css('height','20px');		
			});
		
		});

	</script>



</head>
<body>
<?php
	// $nombreFb = "<script> document.write(sessionStorage.nombre) </script>";
	// $idFb = "<script>document.write(sessionStorage.idfacebook)</script>";
	// $emailFb = "<script>document.write(sessionStorage.email)</script>";	
	conexion();
?>
	<nav>
		<div class="medio">
			<ul>
				<li><a href="#" class="title"><i class="fa fa-lock fa-lg"></i> Notepass</a></li>		
				<li><a href="#" class="setings">Setings</a></li>		
				<li><a href="#" class="logout">Log Out</a></li>
				<!-- <li><div id="user-nameFacebook"></div></li> -->
				<li class="chulo"><div id="user-picture"></div></li>
			</ul>
		</div>
	</nav>	

	<!--
	<div id="referidos">
		<div class="medio">	
			<h3>Now bring your friends and get free space</h3>
			<a class="a-referidos" href="#"><i class="fa fa-users fa-lg"></i> Invite Now</a>
		</div>
	</div>
	-->

	<section>
		<div class="medio">
			<article class="new-account">
				<h1>Have an new account?</h1>				
				<form>
					<label for="user-service">Name or Web Service</label>
					<input type="text" id="user-service" autocompelte="false" placeholder="e.g. http://www.microsoft.com" required/>

					<label for="user-email">Username or Email adress used</label>
					<input type="text" id="user-email" autocompelte="false" placeholder="e.g. paul@microsoft.com" required/>

					<label for="user-password">Your Password</label>
					<input type="password" id="user-password"  required/>

					<input type="submit" value="Save new Account">
				</form>	
					<?php
						if((isset($_POST['user-service'])) && (isset($_POST['user-email'])) && (isset($_POST['user-password']))   ){
							$usuario = $_POST['user'];
							$contrasena = $_POST['pass'];

							if(($usuario!=="") && ($contrasena!=="")){
								if(($usuario==="admin") && ($contrasena==="1234")){
									header('Location: interna/principal.php');
								}else{									
									header("HTTP/1.0 404 Not Found");
								}	
							}

						}
					?>
			</article>

			<h1 class="awesome-title"><i class="fa fa-users fa-lg"></i> My Accounts</h1>

			<article class="list-account">
				<h1>Bytelchus.com</h1>
				<div class="datos">
					<span>Username or Email adress</span>
					<div class="muestro-datos">pablomaio@bytelchus.com</div>

					<span>Password</span>
					<div class="muestro-datos password">unaContr4asen4muyLarga123</div>
				</div>
			</article>

			<article class="list-account">
				<h1>Microsoft Dev</h1>
				<div class="datos">
					<span>Username or Email adress</span>
					<div class="muestro-datos">pablomaio@bytelchus.com</div>

					<span>Password</span>
					<div class="muestro-datos password">123456</div>
				</div>				
			</article>

			<article class="list-account">
				<h1>Developer Facebook</h1>
				<div class="datos">
					<span>Username or Email adress</span>
					<div class="muestro-datos">pablomaio@bytelchus.com</div>

					<span>Password</span>
					<div class="muestro-datos password">C0ntraseña*</div>
				</div>
			</article>
				
			
		</div>
	</section>
	<footer>
		<div class="medio">
			<p>
				<a href="#" class="title"><i class="fa fa-lock fa-lg"></i> Notepass</a>
			</p>
		</div>
	</footer>
</body>
</html>